package com.hegedusviktor;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) {
        int coreCount = Runtime.getRuntime().availableProcessors();
        int elements = 60000000;

        long[] array = generateNumbers(elements);
        Instant start = Instant.now();
        int sumOneThread = addElements(array);
        System.out.println(sumOneThread);
        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println("Elapsed time with one thread with " + elements + " elements: " + timeElapsed);

        Instant startTimeConcurrentTask = Instant.now();
        List<Future<Integer>> futureList = new ArrayList<>();
        ExecutorService service = Executors.newFixedThreadPool(coreCount);
        for (int i = 0; i < coreCount; i++) {
            final Future<Integer> futureTask = service.submit(new CpuTask(i, coreCount, array));
            futureList.add(futureTask);
        }
        Instant finishTimeConcurrentTask = Instant.now();

        int sumFutureList =  futureList.stream().mapToInt(i -> {
            try {
                return i.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            return 0;
        }).sum();
        System.out.println(sumFutureList);

        long timeElapsedConcurrentTask = Duration.between(startTimeConcurrentTask, finishTimeConcurrentTask).toMillis();
        System.out.println("Elapsed time with " + coreCount + " thread with " + elements + " elements: " + timeElapsedConcurrentTask);

        service.shutdown();
    }

    private static int addElements(long[] array) {
        return (int) Arrays.stream(array).sum();
    }

    private static long[] generateNumbers(int elements) {
        Random random = new Random();
        long[] array = new long[elements];
        return Arrays.stream(array).map(i -> random.nextInt(10)).toArray();
    }

    static class CpuTask implements Callable<Integer> {
        private int start;
        private int cores;
        private long[] array;
        private int sum = 0;

        public CpuTask(int start, int cores, long[] array) {
            this.start = start;
            this.cores = cores;
            this.array = array;
        }

        @Override
        public Integer call() {
            for (int i = start*(array.length/cores); i < start*(array.length/cores) + array.length/cores; i++) {
                sum += array[i];
            }
            return sum;
        }
    }

}
